package com.douglasalexandre;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class AlunoEqualize {
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	Long id;
	@Persistent
	String nome;
	@Persistent
	String curso;
	@Persistent
	String nivel;
	@Persistent
	String cursando;
	@Persistent
	String categoria;
	@Persistent
	String valorMensalidade;
	@Persistent
	String emDias;
	@Persistent
	String inicioDoCurso;
	@Persistent
	String terminoDocurso;
	@Persistent
	String dataUltimoPagamento;
	@Persistent
	String observacao;
	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getCurso() {
		return curso;
	}


	public void setCurso(String curso) {
		this.curso = curso;
	}


	public String getNivel() {
		return nivel;
	}


	public void setNivel(String nivel) {
		this.nivel = nivel;
	}


	public String getCursando() {
		return cursando;
	}


	public void setCursando(String cursando) {
		this.cursando = cursando;
	}


	public String getCategoria() {
		return categoria;
	}


	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}


	public String getValorMensalidade() {
		return valorMensalidade;
	}


	public void setValorMensalidade(String valorMensalidade) {
		this.valorMensalidade = valorMensalidade;
	}


	public String getEmDias() {
		return emDias;
	}


	public void setEmDias(String emDias) {
		this.emDias = emDias;
	}


	public String getInicioDoCurso() {
		return inicioDoCurso;
	}


	public void setInicioDoCurso(String inicioDoCurso) {
		this.inicioDoCurso = inicioDoCurso;
	}


	public String getTerminoDocurso() {
		return terminoDocurso;
	}


	public void setTerminoDocurso(String terminoDocurso) {
		this.terminoDocurso = terminoDocurso;
	}


	public String getDataUltimoPagamento() {
		return dataUltimoPagamento;
	}


	public void setDataUltimoPagamento(String dataUltimoPagamento) {
		this.dataUltimoPagamento = dataUltimoPagamento;
	}


	public String getObservacao() {
		return observacao;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


}
