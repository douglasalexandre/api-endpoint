package com.douglasalexandre;

import com.douglasalexandre.PMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Api(name = "alunoequalizeendpoint", namespace = @ApiNamespace(ownerDomain = "douglasalexandre.com", ownerName = "douglasalexandre.com", packagePath=""))
public class AlunoEqualizeEndpoint {

  /**
   * This method lists all the entities inserted in datastore.
   * It uses HTTP GET method and paging support.
   *
   * @return A CollectionResponse class containing the list of all entities
   * persisted and a cursor to the next page.
   */
  @SuppressWarnings({"unchecked", "unused"})
  @ApiMethod(name = "listAlunoEqualize")
  public CollectionResponse<AlunoEqualize> listAlunoEqualize(
    @Nullable @Named("cursor") String cursorString,
    @Nullable @Named("limit") Integer limit) {

    PersistenceManager mgr = null;
    Cursor cursor = null;
    List<AlunoEqualize> execute = null;

    try{
      mgr = getPersistenceManager();
      Query query = mgr.newQuery(AlunoEqualize.class);
      if (cursorString != null && cursorString != "") {
        cursor = Cursor.fromWebSafeString(cursorString);
        HashMap<String, Object> extensionMap = new HashMap<String, Object>();
        extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
        query.setExtensions(extensionMap);
      }

      if (limit != null) {
        query.setRange(0, limit);
      }

      execute = (List<AlunoEqualize>) query.execute();
      cursor = JDOCursorHelper.getCursor(execute);
      if (cursor != null) cursorString = cursor.toWebSafeString();

      // Tight loop for fetching all entities from datastore and accomodate
      // for lazy fetch.
      for (AlunoEqualize obj : execute);
    } finally {
      mgr.close();
    }

    return CollectionResponse.<AlunoEqualize>builder()
      .setItems(execute)
      .setNextPageToken(cursorString)
      .build();
  }

  /**
   * This method gets the entity having primary key id. It uses HTTP GET method.
   *
   * @param id the primary key of the java bean.
   * @return The entity with primary key id.
   */
  @ApiMethod(name = "getAlunoEqualize")
  public AlunoEqualize getAlunoEqualize(@Named("id") Long id) {
    PersistenceManager mgr = getPersistenceManager();
    AlunoEqualize alunoequalize  = null;
    try {
      alunoequalize = mgr.getObjectById(AlunoEqualize.class, id);
    } finally {
      mgr.close();
    }
    return alunoequalize;
  }

  /**
   * This inserts a new entity into App Engine datastore. If the entity already
   * exists in the datastore, an exception is thrown.
   * It uses HTTP POST method.
   *
   * @param alunoequalize the entity to be inserted.
   * @return The inserted entity.
   */
  @ApiMethod(name = "insertAlunoEqualize")
  public AlunoEqualize insertAlunoEqualize(AlunoEqualize alunoequalize) {
    PersistenceManager mgr = getPersistenceManager();
    try {
     if (alunoequalize.getId() != null) {
      if(containsAlunoEqualize(alunoequalize)) {
        throw new EntityExistsException("Object already exists");
      }
     }
      mgr.makePersistent(alunoequalize);
    } finally {
      mgr.close();
    }
    return alunoequalize;
  }

  /**
   * This method is used for updating an existing entity. If the entity does not
   * exist in the datastore, an exception is thrown.
   * It uses HTTP PUT method.
   *
   * @param alunoequalize the entity to be updated.
   * @return The updated entity.
   */
  @ApiMethod(name = "updateAlunoEqualize")
  public AlunoEqualize updateAlunoEqualize(AlunoEqualize alunoequalize) {
    PersistenceManager mgr = getPersistenceManager();
    try {
      if(!containsAlunoEqualize(alunoequalize)) {
        throw new EntityNotFoundException("Object does not exist");
      }
      mgr.makePersistent(alunoequalize);
    } finally {
      mgr.close();
    }
    return alunoequalize;
  }

  /**
   * This method removes the entity with primary key id.
   * It uses HTTP DELETE method.
   *
   * @param id the primary key of the entity to be deleted.
   */
  @ApiMethod(name = "removeAlunoEqualize")
  public void removeAlunoEqualize(@Named("id") Long id) {
    PersistenceManager mgr = getPersistenceManager();
    try {
      AlunoEqualize alunoequalize = mgr.getObjectById(AlunoEqualize.class, id);
      mgr.deletePersistent(alunoequalize);
    } finally {
      mgr.close();
    }
  }

  private boolean containsAlunoEqualize(AlunoEqualize alunoequalize) {
    PersistenceManager mgr = getPersistenceManager();
    boolean contains = true;
    try {
      mgr.getObjectById(AlunoEqualize.class, alunoequalize.getId());
    } catch (javax.jdo.JDOObjectNotFoundException ex) {
      contains = false;
    } finally {
      mgr.close();
    }
    return contains;
  }

  private static PersistenceManager getPersistenceManager() {
    return PMF.get().getPersistenceManager();
  }

}
